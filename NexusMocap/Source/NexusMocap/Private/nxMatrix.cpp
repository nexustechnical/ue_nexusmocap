#include "nxMatrix.h"

FnxMatrix::FnxMatrix()
{
	rows = 0;
	columns = 0;
	//matrix = TArray<double>();
};
FnxMatrix::FnxMatrix(size_t numrows, size_t numcols)
{
	rows = numrows;
	columns = numcols;
	matrix.Init(0.0, numrows * numcols);
}

FnxMatrix::FnxMatrix(TArray<double> vector) {
	rows = 1;
	columns = vector.Num();
	matrix = vector;
}