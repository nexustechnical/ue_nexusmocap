// Fill out your copyright notice in the Description page of Project Settings.


#include "DirectMappingComponent.h"

// Sets default values for this component's properties
UDirectMappingComponent::UDirectMappingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDirectMappingComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	for (FDirectMapping D : DirectMappings) {
		
	}
}


// Called every frame
void UDirectMappingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UDirectMappingComponent::GetNeutralPose()
{
	for (FDirectMapping D : DirectMappings) {
		if (!InData.Contains(D.InputName)) continue;
		D.InputZeroValue = InData[D.InputName];
		D.OutputMultiplier = 1.0f;
	}
}

void UDirectMappingComponent::Update() {
	for (FDirectMapping D : DirectMappings) {
		if (!InData.Contains(D.InputName)) continue;
		float Value = InData[D.InputName];
		Value -= D.InputZeroValue;
		Value *= D.OutputMultiplier;
		Value = FMath::Clamp(Value, 0.0f, 1.0f);
		/*
		if (!KalmanFilters.Contains(D.OutputName)) {
			KalmanFilters.Add(D.OutputName, KalmanFilter());
		}
		Value = KalmanFilters[D.OutputName].Update(Value, KalmanQ, KalmanR);
		*/
		OutData.Add(D.OutputName, Value);
	}
}
