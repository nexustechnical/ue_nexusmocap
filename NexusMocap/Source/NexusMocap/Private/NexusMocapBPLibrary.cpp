// Copyright Epic Games, Inc. All Rights Reserved.

#include "NexusMocapBPLibrary.h"
#include "NexusMocap.h"

DEFINE_LOG_CATEGORY(LogNexusMocap)

float UNexusMocapBPLibrary::KalmanQ = 0.0000001f;
float UNexusMocapBPLibrary::KalmanR = 0.000001f;

UNexusMocapBPLibrary::UNexusMocapBPLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UNexusMocapBPLibrary::SoftIK(FVector rootPosition, FVector ikGoalPosition, float chainLength, float softness, float stretchiness, FVector& ikPosition, float& stretchFactor) {
	FVector targetPos = ikGoalPosition;
	float softdist = softness * chainLength / 2;
	FVector diffPos = targetPos - rootPosition;
	float curlen = diffPos.Size();
	ikPosition = ikGoalPosition;
	float diff = curlen - (chainLength - softdist);

	if (diff > 0 && softdist >= 0.00001)
	{
		diffPos = diffPos * (1.0F / curlen);
		float da = chainLength - softdist;
		float newLength = softdist * (1.0f - FMath::Exp(-(curlen - da) / softdist)) + da;
		diffPos *= newLength;
		targetPos = diffPos + rootPosition;
		ikPosition = targetPos;
	};
	stretchFactor = 1.0f;
	if (stretchiness > 0.0) {
		if (softdist < 0.00001f) {
			stretchFactor = FMath::Clamp(chainLength / curlen, 0.00001f, 1.0f);
		}
		else {
			stretchFactor = FVector::Distance(rootPosition, ikPosition) / FVector().Dist(rootPosition, ikGoalPosition);
		};
	};
	stretchFactor = FMath::Clamp(stretchFactor, 0.00001f, 1.0f);
}

bool UNexusMocapBPLibrary::CalcIK_2D_TwoBoneAnalytic(bool flipSolveDirection, float length1, float length2, float targetX, float targetY, float& angle1, float& angle2) {
	float epsilon = 0.0001f; // used to prevent division by small numbers
	bool foundValidSolution = true;
	float targetDistSqr = (targetX * targetX + targetY * targetY);
	//===
	// Compute a new value for angle2 along with its cosine
	float sinAngle2;
	float cosAngle2;

	float cosAngle2Denom = 2 * length1 * length2;
	if (cosAngle2Denom > epsilon)
	{
		cosAngle2 = (length1 * length1 + length2 * length2 - targetDistSqr) / (cosAngle2Denom);

		// if our result is not in the legal cosine range, we can not find a
		// legal solution for the target
		if ((cosAngle2 < -1.0) || (cosAngle2 > 1.0)) {
			foundValidSolution = false;
		}

		// clamp our value into range so we can calculate the best
		// solution when there are no valid ones

		cosAngle2 = FMath::Max(-1.0f, FMath::Min(1.0f, cosAngle2));

		// compute a new value for anglee2
		angle2 = FMath::Acos(cosAngle2);
		angle2 = PI - angle2;
		// adjust for the desired bend direction
		if (!flipSolveDirection)
			angle2 = -angle2;

		// compute the sine of our angle
		sinAngle2 = FMath::Sin(angle2);
	}
	else
	{
		// At leaset one of the bones had a zero length. This means our
		// solvable domain is a circle around the origin with a radius
		// equal to the sum of our bone lengths.
		float totalLenSqr = (length1 + length2) * (length1 + length2);
		if (targetDistSqr < (totalLenSqr - epsilon) || targetDistSqr >(totalLenSqr + epsilon)) {
			foundValidSolution = false;
		}

		// Only the value of angle1 matters at this point. We can just
		// set angle2 to zero. 
		angle2 = 0.0f;
		cosAngle2 = 1.0f;
		sinAngle2 = 0.0f;
	}

	// Calculate angle1 using the same maths as angle 2
	float cosAngle1 = (length1 * length1 + targetDistSqr - length2 * length2) / (2 * FMath::Sqrt(targetDistSqr)*length1);
	if ((cosAngle1 < -1.0) || (cosAngle1 > 1.0)) {
		foundValidSolution = false;
	}
	cosAngle1 = FMath::Max(-1.0f, FMath::Min(1.0f, cosAngle1));
	angle1 = FMath::Acos(cosAngle1);
	
	//===
	// 
	/*
	// Compute the value of angle1 based on the sine and cosine of angle2
	// This gives me strange results?
	float triAdjacent = length1 + length2 * cosAngle2;
	float triOpposite = length2 * sinAngle2;

	float tanY = targetY * triAdjacent - targetX * triOpposite;
	float tanX = targetX * triAdjacent + targetY * triOpposite;

	// Note that it is safe to call Atan2(0,0) which will happen if targetX and
	// targetY are zero
	angle1 = FMath::Atan2(tanY, tanX);
	*/
	
	return foundValidSolution;
}

FVector UNexusMocapBPLibrary::CalculatePoleVectorLocation(FTransform Root, FTransform Bend, float LocalOffset = 0.1f, FVector LocalDirection = FVector::ForwardVector) {
	return Bend.GetTranslation() + Bend.GetRotation().RotateVector(LocalDirection) * LocalOffset;
}

FVector UNexusMocapBPLibrary::SpaceTransferLocation(FVector Location, FTransform FromSpace, FTransform ToSpace, float ScaleRatio = 1.0f, FRotator Rotation = FRotator::ZeroRotator) {
	Location = FromSpace.ToMatrixNoScale().InverseTransformPosition(Location);
	// Multiply by the arm ratio
	Location = Location * ScaleRatio;
	Location = Rotation.RotateVector(Location);
	// Find the pole vector position in the space of the retarget skeleton's shoulder
	return ToSpace.ToMatrixNoScale().TransformPosition(Location);
}

FVector UNexusMocapBPLibrary::SpaceTransferLocationNonUniform(FVector Location, FTransform FromSpace, FTransform ToSpace, FVector ScaleRatio = FVector::OneVector, FRotator Rotation = FRotator::ZeroRotator) {
	Location = FromSpace.ToMatrixNoScale().InverseTransformPosition(Location);
	// Multiply by the arm ratio
	Location = Location * ScaleRatio;
	Location = Rotation.RotateVector(Location);
	// Find the pole vector position in the space of the retarget skeleton's shoulder
	return ToSpace.ToMatrixNoScale().TransformPosition(Location);
}
FQuat UNexusMocapBPLibrary::LookAt(FVector Direction, FVector UpVector) {
	return FRotationMatrix::MakeFromXZ(Direction, UpVector).ToQuat();
	FVector n1 = Direction.GetSafeNormal();
	FVector n2 = UpVector.GetSafeNormal();
	float d = FVector::DotProduct(n1, n2);
	if (d > 0.9998f) {
		return FQuat(0, 0, 1, 0);
	}
	if (d < -0.9998f) {
		n1 = n1.RotateAngleAxis(180, FVector::RightVector);
	}
	FVector axis = FVector::CrossProduct(n1, n2);
	FQuat pointToTarget = FQuat(1.0f + d, axis.X, axis.Y, axis.Z);
	pointToTarget.Normalize();
	FMatrix projectionMatrix = FMatrix();

}

FMatrix UNexusMocapBPLibrary::RotateAlign(FVector v1, FVector v2)
{
	FVector axis = FVector::CrossProduct(v1, v2);

	const float cosA = FVector::DotProduct(v1, v2);
	const float k = 1.0f / (1.0f + cosA);
	FVector X = FVector((axis.X * axis.X * k) + cosA, (axis.Y * axis.X * k) - axis.Z, (axis.Z * axis.X * k) + axis.Y);
	FVector Y = FVector((axis.X * axis.Y * k) + axis.Z, (axis.Y * axis.Y * k) + cosA, (axis.Z * axis.Y * k) - axis.X);
	FVector Z = FVector((axis.X * axis.Z * k) - axis.Y, (axis.Y * axis.Z * k) + axis.X, (axis.Z * axis.Z * k) + cosA);
	return FMatrix(X, Y, Z, FVector::OneVector);
}

void UNexusMocapBPLibrary::IKSpaceTransfer(FTransform Root, FTransform Bend, FTransform End, FTransform PVFromSpace, FTransform PVToSpace,
	FTransform IKFromSpace, FTransform IKToSpace, FTransform TargetRoot, FTransform TargetBend, FTransform TargetEnd,
	FQuat& RootRotation, FQuat& BendRotation,
	float ScaleRatio, float PoleVectorLocalOffset, FVector RotationAxis, FQuat OffsetRotation) {
	FVector PoleVectorLocation = CalculatePoleVectorLocation(Root, Bend, PoleVectorLocalOffset);
	PoleVectorLocation = SpaceTransferLocation(PoleVectorLocation, PVFromSpace, PVToSpace, ScaleRatio);
	FVector IKGoalPosition = SpaceTransferLocation(End.GetLocation(), IKFromSpace, IKToSpace, ScaleRatio);
	FQuat IKChainRotation = LookAt(IKGoalPosition - Root.GetLocation(), PoleVectorLocation - Root.GetLocation());
	IKGoalPosition = IKChainRotation.Inverse() * (IKGoalPosition - Root.GetLocation());

	float length1 = TargetRoot.InverseTransformPositionNoScale(TargetBend.GetTranslation()).Size();
	float length2 = TargetBend.InverseTransformPositionNoScale(TargetEnd.GetTranslation()).Size();

	// Soften the IK position
	FVector softenedIKPosition = IKGoalPosition;
	float stretchFactor = 0.0f;
	UNexusMocapBPLibrary::SoftIK(FVector::ZeroVector, IKGoalPosition, length1 + length2, 0.01f, 0.0f, softenedIKPosition, stretchFactor);

	// Calculate the IK chain orientations
	float angle1 = 0.0f;
	float angle2 = 0.0f;
	/*
	float x=0.0f;
	float y=0.0f;
	if (FMath::Abs(FVector::DotProduct(FVector::RightVector, RotationAxis)) > 0.5f) {
		x = softenedIKPosition.Z;
		y = softenedIKPosition.X;
	}
	else if (FMath::Abs(FVector::DotProduct(FVector::UpVector, RotationAxis)) > 0.5f) {
		x = softenedIKPosition.Y;
		y = softenedIKPosition.Z;
	}
	else if (FMath::Abs(FVector::DotProduct(FVector::ForwardVector, RotationAxis)) > 0.5f) {
		x = softenedIKPosition.X;
		y = softenedIKPosition.Y;
	}*/
	bool ikSolutionFound = UNexusMocapBPLibrary::CalcIK_2D_TwoBoneAnalytic(true, length1, length2, softenedIKPosition.Y, softenedIKPosition.Z, angle1, angle2);
	if (!ikSolutionFound) {
		UE_LOG(LogTemp, Warning, TEXT("No IK solution found"));
	}
	RootRotation = FQuat().MakeFromEuler(FVector().RotateAngleAxis(FMath().RadiansToDegrees(angle1), RotationAxis));
	BendRotation = FQuat().MakeFromEuler(FVector().RotateAngleAxis(FMath().RadiansToDegrees(angle2), RotationAxis));
	RootRotation = IKChainRotation * OffsetRotation * RootRotation;
}

FRotator UNexusMocapBPLibrary::LookAtRotation(FVector lookDirection, FVector upDirection)
{
	FVector forward = lookDirection;
	FVector up = upDirection;


	forward = forward.GetSafeNormal();
	up = up - (forward * FVector::DotProduct(up, forward));
	up = up.GetSafeNormal();

	///////////////////////


	FVector vector = forward.GetSafeNormal();
	FVector vector2 = FVector::CrossProduct(up, vector);
	FVector vector3 = FVector::CrossProduct(vector, vector2);
	float m00 = vector.X;
	float m01 = vector.Y;
	float m02 = vector.Z;
	float m10 = vector2.X;
	float m11 = vector2.Y;
	float m12 = vector2.Z;
	float m20 = vector3.X;
	float m21 = vector3.Y;
	float m22 = vector3.Z;

	float num8 = (m00 + m11) + m22;
	FQuat quaternion = FQuat();

	if (num8 > 0.0f)
	{
		float num = (float)FMath::Sqrt(num8 + 1.0f);
		quaternion.W = num * 0.5f;
		num = 0.5f / num;
		quaternion.X = (m12 - m21) * num;
		quaternion.Y = (m20 - m02) * num;
		quaternion.Z = (m01 - m10) * num;
		return FRotator(quaternion);
	}

	if ((m00 >= m11) && (m00 >= m22))
	{
		float num7 = (float)FMath::Sqrt(((1.0f + m00) - m11) - m22);
		float num4 = 0.5f / num7;
		quaternion.X = 0.5f * num7;
		quaternion.Y = (m01 + m10) * num4;
		quaternion.Z = (m02 + m20) * num4;
		quaternion.W = (m12 - m21) * num4;
		return FRotator(quaternion);
	}

	if (m11 > m22)
	{
		float num6 = (float)FMath::Sqrt(((1.0f + m11) - m00) - m22);
		float num3 = 0.5f / num6;
		quaternion.X = (m10 + m01) * num3;
		quaternion.Y = 0.5f * num6;
		quaternion.Z = (m21 + m12) * num3;
		quaternion.W = (m20 - m02) * num3;
		return FRotator(quaternion);
	}

	float num5 = (float)FMath::Sqrt(((1.0f + m22) - m00) - m11);
	float num2 = 0.5f / num5;
	quaternion.X = (m20 + m02) * num2;
	quaternion.Y = (m21 + m12) * num2;
	quaternion.Z = 0.5f * num5;
	quaternion.W = (m01 - m10) * num2;


	return FRotator(quaternion);
}

void UNexusMocapBPLibrary::SetBoneWorldTransform(UProxySkeleton *ProxySkeleton, FName BoneName, FTransform Transform) {
	ProxySkeleton->SetWorldTransform(BoneName, Transform);
}

FTransform UNexusMocapBPLibrary::GetBoneWorldTransform(UProxySkeleton *ProxySkeleton, FName BoneName) {
	return ProxySkeleton->GetWorldTransform(BoneName);
}

void UNexusMocapBPLibrary::SetBoneLocalTransform(UProxySkeleton* ProxySkeleton, FName BoneName, FTransform Transform) {
	ProxySkeleton->SetLocalTransform(BoneName, Transform);
}

FTransform UNexusMocapBPLibrary::GetBoneLocalTransform(UProxySkeleton* ProxySkeleton, FName BoneName) {
	return ProxySkeleton->GetLocalTransform(BoneName);
}

void UNexusMocapBPLibrary::GetBoneTransform(UProxySkeleton *ProxySkeleton, FName BoneName, FTransform& WorldSpaceTransform, FTransform& LocalSpaceTransform) {
	WorldSpaceTransform = ProxySkeleton->GetWorldTransform(BoneName);
	LocalSpaceTransform = ProxySkeleton->GetLocalTransform(BoneName);
}

void UNexusMocapBPLibrary::InitProxySkeleton(UProxySkeleton* ProxySkeleton) {
	//ProxySkeleton->SetupTransformsByName();
	//ProxySkeleton->SetupParents();
}

void UNexusMocapBPLibrary::DrawProxySkeleton(UProxySkeleton* ProxySkeleton, FColor Color=FColor::Black) {
	ProxySkeleton->DrawDebugSkeleton(Color);
}

FTransform UNexusMocapBPLibrary::GetBoneParentTransform(UProxySkeleton* ProxySkeleton, FName BoneName) {
	return ProxySkeleton->GetParentTransform(BoneName);
}
FName UNexusMocapBPLibrary::GetBoneParent(UProxySkeleton* ProxySkeleton, FName BoneName) {
	return ProxySkeleton->GetParent(BoneName);
}

TArray<FName> UNexusMocapBPLibrary::GetBoneChildren(UProxySkeleton* ProxySkeleton, FName BoneName) {
	return ProxySkeleton->GetChildren(BoneName);
}

void UNexusMocapBPLibrary::SetProxyFromAnimSequence(UProxySkeleton* ProxySkeleton, UAnimSequence* TPose) {
	//TPose->GetPreviewMesh();
	//TPose->mesh


	FCompactPose OutPose;
	FBlendedCurve OutCurve;
	FStackCustomAttributes attributes;
	FAnimationPoseData outAnimationPoseData(OutPose, OutCurve, attributes);
	TPose->GetAnimationPose(outAnimationPoseData, FAnimExtractContext(0, true));
	//TArray<FTransform, FAnimStackAllocator> bones = OutPose.GetBones();
	/*
	for (int32 i = 0; i < TPose.Num(); ++i) {
		OutPose.GetBoneContainer().GetReferenceSkeleton().GetParentIndex(i)
	}
	FTransform BoneTransform;
	TPose->GetBoneTransform(BoneTransform, 0, 0.0f, true);

	*/

}

void UNexusMocapBPLibrary::GetJointRotationsInTPose(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton, TMap<FName, FQuat>& JointRotations) {
	for (TPair<FName, FTransform> ProxyBone : ProxySkeleton->BoneLocalTransforms) {
		FName BoneName = ProxyBone.Key;
		FTransform TPoseBoneTransform = GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, BoneName, 0.0f);
		JointRotations.Add(BoneName, TPoseBoneTransform.GetRotation());
	}
}

FQuat UNexusMocapBPLibrary::GetJointWorldRotationInTPose(FName BoneName, USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton) {
	UE_LOG(LogNexusMocap, Log, TEXT("=GetJointWorldRotationInTPose="))
	if (SkeletalMeshComponent == nullptr) {
		UE_LOG(LogNexusMocap, Warning, TEXT("GetJointWorldRotationInTPose: SkeletalMeshComponent is null"))
			return FQuat::Identity;
	}
	if (AnimSequence == nullptr) {
		UE_LOG(LogNexusMocap, Warning, TEXT("GetJointWorldRotationInTPose: AnimSequence is null"))
			return FQuat::Identity;
	}
	if (ProxySkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Warning, TEXT("GetJointWorldRotationInTPose: ProxySkeleton is null"))
			return FQuat::Identity;
	}
	
	//UE_LOG(LogNexusMocap, Log, TEXT("BoneName: %s"), *BoneName.ToString())
	FQuat BoneRotation = GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, BoneName, 0.0f).GetRotation();
	//UE_LOG(LogNexusMocap, Log, TEXT("Bone rotation from animation sequence: %s"), *BoneRotation.ToString())
	FName ParentBoneName = ProxySkeleton->GetParent(BoneName);
	//UE_LOG(LogNexusMocap, Log, TEXT("ParentBone: %s"), *ParentBoneName.ToString())
	UE_LOG(LogNexusMocap, Log, TEXT("Parent: %s -> Bone: %s"), *ParentBoneName.ToString(), *BoneName.ToString())
	if (ParentBoneName == BoneName) {
		UE_LOG(LogNexusMocap, Log, TEXT("Should be Reference"))
		return BoneRotation;
	}
	FQuat ParentBoneRotation = GetJointWorldRotationInTPose(ParentBoneName, SkeletalMeshComponent, AnimSequence, ProxySkeleton);
	//UE_LOG(LogNexusMocap, Log, TEXT("Parent Bone rotation from animation sequence: %s"), *ParentBoneRotation.ToString())
	return ParentBoneRotation * BoneRotation;
	// return BoneRotation * ParentBoneRotation;
}

void UNexusMocapBPLibrary::GetJointWorldRotationsInTPose(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton, TMap<FName, FQuat>& JointRotations) {
	for (TPair<FName, FTransform> ProxyBone : ProxySkeleton->BoneLocalTransforms) {
		FName BoneName = ProxyBone.Key;
		JointRotations.Add(BoneName, GetJointWorldRotationInTPose(BoneName, SkeletalMeshComponent, AnimSequence, ProxySkeleton));
	}
}

void UNexusMocapBPLibrary::GetChainToBone(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName, FName ParentBoneName, TArray<FName>& Chain, bool& Found) {

	FName Next = BoneName;
	int counter = 0;
	while (Next != ParentBoneName && Next != FName("Reference") && counter < 100) {
		Chain.Add(Next);
		Next = SkeletalMeshComponent->GetParentBone(Next);
		
		counter++;
	}
	Found = Next == ParentBoneName;
}

FTransform UNexusMocapBPLibrary::GetLocalTransformFromAnimSequence(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, FName BoneName, float AtTime = 0.0f) {
	

	if (SkeletalMeshComponent == nullptr) {
		UE_LOG(LogNexusMocap, Warning, TEXT("GetLocalTransformFromAnimSequence: SkeletalMeshComponent is null"))
			return FTransform::Identity;
	}
	if (AnimSequence == nullptr) {
		UE_LOG(LogNexusMocap, Warning, TEXT("GetLocalTransformFromAnimSequence: AnimSequence is null"))
			return FTransform::Identity;
	}
	FReferenceSkeleton RefSkeleton = SkeletalMeshComponent->SkeletalMesh->RefSkeleton;
	int32 BoneIndex = RefSkeleton.FindBoneIndex(BoneName);
	FStackCustomAttributes attributes;
	// FAnimationPoseData outAnimationPoseData(;
	//AnimSequence->GetAnimationPose(outAnimationPoseData, FAnimExtractContext(0, true));
	if (BoneIndex != INDEX_NONE) {
		return RefSkeleton.GetRefBonePose()[BoneIndex];
	}
	return FTransform();
	FTransform BoneTrans;
	int32 BoneIdx = GetAnimSequenceBoneIndex(AnimSequence, SkeletalMeshComponent, BoneName);
	
	if (BoneIdx != INDEX_NONE)
	{
		return GetBoneTransformAtTime(AnimSequence, AtTime, BoneIdx);
		//int32 const TrackIndex = AnimSequence->GetSkeleton()->GetRawAnimationTrackIndex(BoneIdx, AnimSequence);
		//int32 const TrackIndex = AnimSequence->GetSkeleton()->GetRawAnimationTrackIndex(BoneIdx, AnimSequence);
		//This method can produce invalid indicies, actually, all are wrong, so it's the wrong way
		//int32 TrackIndex = AnimSequence->GetSkeleton()->GetSkeletonBoneIndexFromMeshBoneIndex(SkeletalMeshComponent->SkeletalMesh, BoneIdx);
		// Need to ensure that the animation sequence is valid... but they share a skeleton so it _must_ be valid right?
		// Maybe just check the index is valid. It'd be nice if try/catch worked...
		//int32 const TrackIndex = AnimSequence->GetSkeleton()->GetAnimationTrackIndex(BoneIdx, AnimSequence);
		/*
		if(TrackIndex != INDEX_NONE){
			AnimSequence->GetBoneTransform(BoneTrans, TrackIndex, AtTime, true);
		}
		else{
			UE_LOG(LogNexusMocap, Warning, TEXT("GetLocalTransformFromAnimSequence: Failed to get transform for Bone: %s at Bone index: %d, Track index: %d"), *BoneName.ToString(), BoneIdx, TrackIndex)
		}
		*/
	}
	return BoneTrans;
}

FTransform UNexusMocapBPLibrary::GetWorldTransformFromAnimSequence(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, FName BoneName, float AtTime)
{
	FName ParentBoneName = SkeletalMeshComponent->GetParentBone(BoneName);
	if (ParentBoneName.IsNone()) {
		return GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, BoneName, AtTime);
	}
	FTransform ParentWorldTransform = GetWorldTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, ParentBoneName, AtTime);
	FTransform LocalTransform = GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, BoneName, AtTime);
	// return ParentWorldTransform * LocalTransform;
	return LocalTransform * ParentWorldTransform;
}

int32 UNexusMocapBPLibrary::GetAnimSequenceBoneIndex(UAnimSequence* MyAnimSequence, USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName)
{
	int32 SkeletalMeshBoneIndex = SkeletalMeshComponent->GetBoneIndex(BoneName);
	if (SkeletalMeshBoneIndex != INDEX_NONE) {
		return MyAnimSequence->GetSkeleton()->GetSkeletonBoneIndexFromMeshBoneIndex(SkeletalMeshComponent->SkeletalMesh, SkeletalMeshBoneIndex);
	}
	return INDEX_NONE;
}

FTransform UNexusMocapBPLibrary::GetBoneTransformAtTime(UAnimSequence* MyAnimSequence, float AnimTime, int BoneIdx)
{
	const TArray<FTrackToSkeletonMap>& TrackToSkeletonMap = MyAnimSequence->GetCompressedTrackToSkeletonMapTable();

	FTransform BoneTransform;
	if ((TrackToSkeletonMap.Num() > 0) && (TrackToSkeletonMap[0].BoneTreeIndex == 0))
	{	
		MyAnimSequence->GetBoneTransform(BoneTransform, BoneIdx, AnimTime, false);
	}
	return BoneTransform;
}

float UNexusMocapBPLibrary::GetHipsRatio(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton) {
	FTransform ReferenceTransform = GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, "Reference");
	// UE_LOG(LogNexusMocap, Log, TEXT("ReferenceTransform: %s"), ReferenceTransform.ToString());
	FTransform HipsTransform = GetLocalTransformFromAnimSequence(SkeletalMeshComponent, AnimSequence, "Hips");
	// UE_LOG(LogNexusMocap, Log, TEXT("HipsTransform: %s"), HipsTransform.ToString());
	FVector HipsLocalTranslation = ReferenceTransform.InverseTransformPositionNoScale(HipsTransform.GetTranslation());
	// Makes the assumption that Hip height will be the largest component
	float TargetHipHeight = FMath::Max3(HipsLocalTranslation.X, HipsLocalTranslation.Y, HipsLocalTranslation.Z);
	UE_LOG(LogNexusMocap, Log, TEXT("Target Hip Height: %f"), TargetHipHeight);
	float SourceHipHeight = ProxySkeleton->GetWorldTransformTPose("Reference").InverseTransformPositionNoScale(ProxySkeleton->GetWorldTransformTPose("Hips").GetTranslation()).Z;
	UE_LOG(LogNexusMocap, Log, TEXT("Source Hip Height: %f"), SourceHipHeight);
	if (SourceHipHeight == 0) return 0.0f;
	return TargetHipHeight / SourceHipHeight;
}

void UNexusMocapBPLibrary::SetProxyFromSkeletalMesh(UProxySkeleton* ProxySkeleton, USkeletalMeshComponent* SkeletalMeshComponent) {
	TArray<FName> OutBoneNames;
	SkeletalMeshComponent->GetBoneNames(OutBoneNames);

	for (FName BoneName : OutBoneNames) {
		FName ParentBone = GetBoneParent(ProxySkeleton, BoneName);
		int32 BoneIndex = SkeletalMeshComponent->GetBoneIndex(BoneName);
		int32 ParentBoneIndex = SkeletalMeshComponent->GetBoneIndex(ParentBone);
		FTransform BoneWorldTransform = SkeletalMeshComponent->GetBoneTransform(BoneIndex);
		
		FTransform ParentBoneWorldTransform = SkeletalMeshComponent->GetBoneTransform(ParentBoneIndex);
		FTransform BoneLocalTransform = ParentBoneWorldTransform.Inverse() * BoneWorldTransform;
		float BoneLength = BoneLocalTransform.GetLocation().Size();

		ProxySkeleton->SetLocalTransform(BoneName, ParentBoneWorldTransform.Inverse()*BoneWorldTransform);
	}	
}

void UNexusMocapBPLibrary::SetProxyBoneLengthFromSkeletalMesh(UProxySkeleton* ProxySkeleton, USkeletalMeshComponent* SkeletalMeshComponent, TMap<FName, float>& BoneLengthRatios, UAnimSequence* TPoseAnimation) {
	TArray<FName> OutBoneNames;
	SkeletalMeshComponent->GetBoneNames(OutBoneNames);
	FTransformArrayA2 BoneSpaceTransforms = SkeletalMeshComponent->GetBoneSpaceTransforms();
	for (FName BoneName : OutBoneNames) {
		FName ParentBone = GetBoneParent(ProxySkeleton, BoneName);
		
		//UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *(BoneName.ToString()),  *(ParentBone.ToString()));
		int32 BoneIndex = SkeletalMeshComponent->GetBoneIndex(BoneName);
		int32 ParentBoneIndex = SkeletalMeshComponent->GetBoneIndex(ParentBone);

		FTransform BoneWorldTransform = SkeletalMeshComponent->GetBoneTransform(BoneIndex);
		FTransform ParentBoneWorldTransform = SkeletalMeshComponent->GetBoneTransform(ParentBoneIndex);
		FTransform BoneLocalTransform = BoneWorldTransform * ParentBoneWorldTransform.Inverse();
		float NewBoneLength = BoneSpaceTransforms[BoneIndex].GetTranslation().Size();
		float OldBoneLength = ProxySkeleton->GetLocalTransform(BoneName).GetTranslation().Size();
		float BoneLengthRatio = (NewBoneLength / OldBoneLength);
		if (BoneLengthRatio <= 0.0f) BoneLengthRatio = 1.0f;
		BoneLengthRatios.Add(BoneName, BoneLengthRatio);
		FTransform ProxyTransform = ProxySkeleton->GetLocalTransform(BoneName);
		ProxyTransform.SetTranslation(ProxyTransform.GetTranslation() * BoneLengthRatio);
		ProxySkeleton->SetLocalTransform(BoneName, ProxyTransform);

		//ProxySkeleton->SetLocalTransform(BoneName, ParentBoneWorldTransform.Inverse() * BoneWorldTransform);
	}
}

float UNexusMocapBPLibrary::GetArmLength(UProxySkeleton* ProxySkeleton) {
	FVector Spine3Location = ProxySkeleton->GetWorldTransformTPose("Spine3").GetTranslation();
	FVector LeftShoulderLocation = ProxySkeleton->GetWorldTransformTPose("LeftShoulder").GetTranslation();
	FVector LeftArmLocation = ProxySkeleton->GetWorldTransformTPose("LeftArm").GetTranslation();
	FVector LeftForeArmLocation = ProxySkeleton->GetWorldTransformTPose("LeftForeArm").GetTranslation();
	FVector LeftHandLocation = ProxySkeleton->GetWorldTransformTPose("LeftHand").GetTranslation();
	return FMath::Abs(LeftShoulderLocation.X - Spine3Location.X) + 
		(LeftArmLocation - LeftShoulderLocation).Size() + 
		(LeftForeArmLocation - LeftArmLocation).Size() + 
		(LeftHandLocation - LeftForeArmLocation).Size();
}

void UNexusMocapBPLibrary::GetFootHeightRatio(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, float& SourceFootHeight, float& FootHeightRatio)
{
	if (SourceSkeleton == nullptr || TargetSkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("Input Skeleton null"));
		return;
	}
	FVector SourceFootTranslation = SourceSkeleton->GetWorldTransformTPose("Reference").InverseTransformPosition(SourceSkeleton->GetWorldTransformTPose("LeftFoot").GetTranslation());
	FVector TargetFootTranslation = TargetSkeleton->GetWorldTransformTPose("Reference").InverseTransformPosition(TargetSkeleton->GetWorldTransformTPose("LeftFoot").GetTranslation());
	SourceFootHeight = SourceFootTranslation.Z;
	if (SourceFootHeight == 0) {
		return;
	}
	FootHeightRatio = TargetFootTranslation.Z / SourceFootTranslation.Z;
}

void UNexusMocapBPLibrary::IKSpaceTransferLeftHand(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha,
	float& ArmRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float& StretchFactor, bool DebugDraw, bool FlipSolveDirection, bool InvertForwardAxis, bool InvertUpAxis, bool InvertAngle1, UKalmanFilterVector* KalmanFilter)
{

	if (SourceSkeleton == nullptr || TargetSkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("Input Skeleton null"));
		return;
	}
	
	if (KalmanFilter == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("KalmanFilter is null"));
	}
	
	UWorld* World = SourceSkeleton->GetWorld();
	// Calculate Arm Ratio
	float TargetArmLength = GetArmLength(TargetSkeleton);
	float SourceArmLength = GetArmLength(SourceSkeleton);
	check(SourceArmLength != 0);
	ArmRatio = TargetArmLength / SourceArmLength;

	// Calculate arm lengths
	float ArmLength1 = (TargetSkeleton->GetWorldTransform("LeftForeArm").GetTranslation() - TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation()).Size();
	float ArmLength2 = (TargetSkeleton->GetWorldTransform("LeftHand").GetTranslation() - TargetSkeleton->GetWorldTransform("LeftForeArm").GetTranslation()).Size();

	// LEFT ARM
	//Pole Vector in +X
	PoleVector = SourceSkeleton->GetWorldTransform("LeftForeArm").GetTranslation() + SourceSkeleton->GetWorldTransform("LeftForeArm").GetRotation().RotateVector(PoleVectorLocalOffset);
	if (DebugDraw) DrawDebugLine(World, SourceSkeleton->GetWorldTransform("LeftArm").GetTranslation(), PoleVector, FColor::Green, false, 0.0f, 0, 0.1f);
	PoleVector = SpaceTransferLocation(
		PoleVector, 
		SourceSkeleton->GetWorldTransform("LeftShoulder"), 
		TargetSkeleton->GetWorldTransform("LeftShoulder"), 
		ArmRatio);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation(), PoleVector, FColor::Green, false, 0.0f, 0, 0.1f);
	// Space Transfer
	IKGoal = SpaceTransferLocation(
		SourceSkeleton->GetWorldTransform("LeftHand").GetTranslation(), 
		SourceSkeleton->GetWorldTransform("Spine3"), 
		TargetSkeleton->GetWorldTransform("Spine3"), 
		ArmRatio);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation(), IKGoal, FColor::Green, false, 0.0f, 0, 0.1f);
	
	// Kalman Filter
	FVector FilteredIKGoal = IKGoal; // KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);
	//FVector FilteredIKGoal = KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);

	IKGoal = FMath::Lerp(IKGoal, FilteredIKGoal, KalmanAlpha);

	// LookAt -Y Forward -X Up
	
	FMatrix IChainRotation = FRotationMatrix::MakeFromYX(
		(IKGoal - TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation()) * (InvertForwardAxis ? -1 : 1), 
		(PoleVector - TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation()) * (InvertUpAxis ? -1 : 1));

	FVector RotatedIKGoal = IChainRotation.Inverse().TransformVector((IKGoal - TargetSkeleton->GetWorldTransform("LeftArm").GetTranslation()));

	FVector SoftenedIKGoal;
	SoftIK(FVector::ZeroVector, RotatedIKGoal, ArmLength1 + ArmLength2, IKSoftness, Stretchiness, SoftenedIKGoal, StretchFactor);

	float Angle1 = 0.0f;
	float Angle2 = 0.0f;
	IKSolutionFound = CalcIK_2D_TwoBoneAnalytic(FlipSolveDirection, ArmLength1, ArmLength2, SoftenedIKGoal.Y, SoftenedIKGoal.X, Angle1, Angle2);

	FQuat Rotation1 = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, FMath::RadiansToDegrees(Angle1) * (InvertAngle1 ? -1 : 1)));
	FQuat Rotation2 = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, FMath::RadiansToDegrees(Angle2)));

	TargetSkeleton->SetWorldRotation("LeftArm", IChainRotation.ToQuat() * Rotation1);
	TargetSkeleton->SetLocalRotation("LeftForeArm", Rotation2);
	TargetSkeleton->SetWorldRotation("LeftHand", SourceSkeleton->GetWorldTransform("LeftHand").GetRotation());
}


void UNexusMocapBPLibrary::IKSpaceTransferRightHand(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha,
	float& ArmRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float& StretchFactor, bool DebugDraw, bool FlipSolveDirection, bool InvertForwardAxis, bool InvertUpAxis, bool InvertAngle1, UKalmanFilterVector* KalmanFilter)
{	

	if (SourceSkeleton == nullptr || TargetSkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("Input Skeleton null"));
		return;
	}
	
	if (KalmanFilter == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("KalmanFilter is null"));
	}
	
	UWorld* World = SourceSkeleton->GetWorld();

	// Calculate Arm Ratio
	float TargetArmLength = GetArmLength(TargetSkeleton);
	float SourceArmLength = GetArmLength(SourceSkeleton);
	check(SourceArmLength != 0);
	ArmRatio = TargetArmLength / SourceArmLength;

	// Calculate arm lengths
	float ArmLength1 = (TargetSkeleton->GetWorldTransform("RightForeArm").GetTranslation() - TargetSkeleton->GetWorldTransform("RightArm").GetTranslation()).Size();
	float ArmLength2 = (TargetSkeleton->GetWorldTransform("RightHand").GetTranslation() - TargetSkeleton->GetWorldTransform("RightForeArm").GetTranslation()).Size();

	// Right ARM
	//Pole Vector in +X
	PoleVector = SourceSkeleton->GetWorldTransform("RightForeArm").GetTranslation() + SourceSkeleton->GetWorldTransform("RightForeArm").GetRotation().RotateVector(PoleVectorLocalOffset);
	if (DebugDraw) DrawDebugLine(World, SourceSkeleton->GetWorldTransform("RightArm").GetTranslation(), PoleVector, FColor::Red, false, 0.0f, 0, 0.1f);
	PoleVector = SpaceTransferLocation(
		PoleVector,
		SourceSkeleton->GetWorldTransform("RightShoulder"),
		TargetSkeleton->GetWorldTransform("RightShoulder"),
		ArmRatio);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("RightArm").GetTranslation(), PoleVector, FColor::Red, false, 0.0f, 0, 0.1f);

	// Space Transfer
	IKGoal = SpaceTransferLocation(
		SourceSkeleton->GetWorldTransform("RightHand").GetTranslation(),
		SourceSkeleton->GetWorldTransform("Spine3"),
		TargetSkeleton->GetWorldTransform("Spine3"),
		ArmRatio);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("RightArm").GetTranslation(), IKGoal, FColor::Red, false, 0.0f, 0, 0.1f);
	
	// Kalman Filter
	FVector FilteredIKGoal = IKGoal; // KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);
	//FVector FilteredIKGoal = KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);

	IKGoal = FMath::Lerp(IKGoal, FilteredIKGoal, KalmanAlpha);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("RightArm").GetTranslation(), IKGoal, FColor::Red, false, 0.0f, 0, 0.1f);
	// LookAt -Y Forward -X Up
	FMatrix IChainRotation = FRotationMatrix::MakeFromYX(
		(IKGoal - TargetSkeleton->GetWorldTransform("RightArm").GetTranslation()) * (InvertForwardAxis ? -1 : 1),
		(PoleVector - TargetSkeleton->GetWorldTransform("RightArm").GetTranslation()) * (InvertUpAxis ? -1 : 1));

	FVector RotatedIKGoal = IChainRotation.Inverse().TransformVector((IKGoal - TargetSkeleton->GetWorldTransform("RightArm").GetTranslation()));

	FVector SoftenedIKGoal;
	SoftIK(FVector::ZeroVector, RotatedIKGoal, ArmLength1 + ArmLength2, IKSoftness, Stretchiness, SoftenedIKGoal, StretchFactor);
	if (DebugDraw) DrawDebugLine(World, TargetSkeleton->GetWorldTransform("RightArm").GetTranslation(), IChainRotation.TransformVector(SoftenedIKGoal) + TargetSkeleton->GetWorldTransform("RightArm").GetTranslation(), FColor::Red, false, 0.0f, 0, 0.1f);
	float Angle1 = 0.0f;
	float Angle2 = 0.0f;
	IKSolutionFound = CalcIK_2D_TwoBoneAnalytic(FlipSolveDirection, ArmLength1, ArmLength2, SoftenedIKGoal.Y, SoftenedIKGoal.X, Angle1, Angle2);

	FQuat Rotation1 = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, FMath::RadiansToDegrees(Angle1) * (InvertAngle1 ? -1 : 1)));
	FQuat Rotation2 = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, FMath::RadiansToDegrees(Angle2)));

	TargetSkeleton->SetWorldRotation("RightArm", IChainRotation.ToQuat() *  Rotation1);
	TargetSkeleton->SetLocalRotation("RightForeArm", Rotation2);
	TargetSkeleton->SetWorldRotation("RightHand", SourceSkeleton->GetWorldTransform("RightHand").GetRotation());
}


void UNexusMocapBPLibrary::IKSpaceTransferLeftFoot(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha, float HipsRatio,
	float FootHeightRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float FootHeightFalloff, float SourceFootHeight, bool DebugDraw, UKalmanFilterVector* KalmanFilter)
{

	if (SourceSkeleton == nullptr || TargetSkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("Input Skeleton null"));
		return;
	}
	
	if (KalmanFilter == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("KalmanFilter is null"));
	}
	
	// Calculate leg lengths
	float LegLength1 = (TargetSkeleton->GetWorldTransform("LeftLeg").GetTranslation() - TargetSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation()).Size();
	float LegLength2 = (TargetSkeleton->GetWorldTransform("LeftFoot").GetTranslation() - TargetSkeleton->GetWorldTransform("LeftLeg").GetTranslation()).Size();


	// LEFT LEG
	// Pole Vector in +Y
	PoleVector = SourceSkeleton->GetWorldTransform("LeftLeg").GetTranslation() + SourceSkeleton->GetWorldTransform("LeftLeg").GetRotation().RotateVector(PoleVectorLocalOffset);
	if (DebugDraw) DrawDebugLine(SourceSkeleton->GetWorld(), SourceSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation(), PoleVector, FColor::Green, false, 0.0f, 0, 0.1f);
	PoleVector = SpaceTransferLocation(PoleVector, SourceSkeleton->GetWorldTransform("Reference"), TargetSkeleton->GetWorldTransform("Reference"), HipsRatio);
	if (DebugDraw) DrawDebugLine(TargetSkeleton->GetWorld(), TargetSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation(), PoleVector, FColor::Green, false, 0.0f, 0, 0.1f);

	// Space Transfer Left Foot 
	IKGoal = SourceSkeleton->GetWorldTransform("Reference").InverseTransformPositionNoScale(SourceSkeleton->GetWorldTransform("LeftFoot").GetTranslation());
	float CloseToAnkleHeightHeuristic = FMath::Clamp(1.0f - FMath::Abs(IKGoal.Z - SourceFootHeight) * FootHeightFalloff, 0.0f, 1.0f);
	IKGoal *= FVector(HipsRatio, HipsRatio, FMath::Lerp(HipsRatio, FootHeightRatio, CloseToAnkleHeightHeuristic));
	IKGoal = TargetSkeleton->GetWorldTransform("Reference").TransformPositionNoScale(IKGoal);

	// Kalman Filter Left Foot
	FVector FilteredIKGoal = IKGoal; // KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);
	//FVector FilteredIKGoal = KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);
	
	IKGoal = FMath::Lerp(IKGoal, FilteredIKGoal, KalmanAlpha);

	// LookAt -Z Forward +X Up
	FMatrix IKChainRotation = FRotationMatrix::MakeFromZX(
		(IKGoal - TargetSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation()) * -1, 
		(PoleVector - TargetSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation()));

	FVector RotatedLeftFootIKGoal = IKChainRotation.Inverse().TransformVector((IKGoal - TargetSkeleton->GetWorldTransform("LeftUpLeg").GetTranslation()));
	FVector SoftenedLeftFootIKGoal;
	float LeftUpLegStretchFactor;
	SoftIK(FVector::ZeroVector, RotatedLeftFootIKGoal, LegLength1 + LegLength2, IKSoftness, 0.0f, SoftenedLeftFootIKGoal, LeftUpLegStretchFactor);

	float LeftUpLegAngle = 0.0f;
	float LeftLegAngle = 0.0f;
	IKSolutionFound = CalcIK_2D_TwoBoneAnalytic(false, LegLength1, LegLength2, SoftenedLeftFootIKGoal.Z, SoftenedLeftFootIKGoal.X, LeftUpLegAngle, LeftLegAngle);

	FQuat LeftUpLegRotation = FQuat::MakeFromEuler(FVector(0.0f, FMath::RadiansToDegrees(LeftUpLegAngle), 0.0f));
	FQuat LeftLegRotation = FQuat::MakeFromEuler(FVector(0.0f, FMath::RadiansToDegrees(LeftLegAngle), 0.0f));

	TargetSkeleton->SetWorldRotation("LeftUpLeg", IKChainRotation.ToQuat() * LeftUpLegRotation);
	TargetSkeleton->SetLocalRotation("LeftLeg", LeftLegRotation);
	TargetSkeleton->SetWorldRotation("LeftFoot", SourceSkeleton->GetWorldTransform("LeftFoot").GetRotation());
}


void UNexusMocapBPLibrary::IKSpaceTransferRightFoot(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha, float HipsRatio,
	float FootHeightRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float FootHeightFalloff, float SourceFootHeight, bool DebugDraw, UKalmanFilterVector* KalmanFilter)
{

	if (SourceSkeleton == nullptr || TargetSkeleton == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("Input Skeleton null"));
		return;
	}
	
	if (KalmanFilter == nullptr) {
		UE_LOG(LogNexusMocap, Log, TEXT("KalmanFilter is null"));
	}
	
	// Calculate leg lengths
	float LegLength1 = (TargetSkeleton->GetWorldTransform("RightLeg").GetTranslation() - TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation()).Size();
	float LegLength2 = (TargetSkeleton->GetWorldTransform("RightFoot").GetTranslation() - TargetSkeleton->GetWorldTransform("RightLeg").GetTranslation()).Size();


	// Right LEG
	// Pole Vector in +Y
	PoleVector = SourceSkeleton->GetWorldTransform("RightLeg").GetTranslation() + SourceSkeleton->GetWorldTransform("RightLeg").GetRotation().RotateVector(PoleVectorLocalOffset);
	if (DebugDraw) DrawDebugLine(SourceSkeleton->GetWorld(), SourceSkeleton->GetWorldTransform("RightUpLeg").GetTranslation(), PoleVector, FColor::Red, false, 0.0f, 0, 0.1f);
	PoleVector = SpaceTransferLocation(PoleVector, SourceSkeleton->GetWorldTransform("Reference"), TargetSkeleton->GetWorldTransform("Reference"), HipsRatio);
	if (DebugDraw) DrawDebugLine(TargetSkeleton->GetWorld(), TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation(), PoleVector, FColor::Red, false, 0.0f, 0, 0.1f);

	// Space Transfer Right Foot 
	IKGoal = SourceSkeleton->GetWorldTransform("Reference").InverseTransformPositionNoScale(SourceSkeleton->GetWorldTransform("RightFoot").GetTranslation());
	float CloseToAnkleHeightHeuristic = FMath::Clamp(1.0f - FMath::Abs(IKGoal.Z - SourceFootHeight) * FootHeightFalloff, 0.0f, 1.0f);
	IKGoal *= FVector(HipsRatio, HipsRatio, FMath::Lerp(HipsRatio, FootHeightRatio, CloseToAnkleHeightHeuristic));
	IKGoal = TargetSkeleton->GetWorldTransform("Reference").TransformPositionNoScale(IKGoal);

	if (DebugDraw) DrawDebugLine(TargetSkeleton->GetWorld(), TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation(), IKGoal, FColor::Red, false, 0.0f, 0, 0.1f);

	// Kalman Filter Right Foot
	FVector FilteredIKGoal = IKGoal; // KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);
	//FVector FilteredIKGoal = KalmanFilter->Update(IKGoal, KalmanQ, KalmanR);

	//if (DebugDraw) DrawDebugPoint(TargetSkeleton->GetWorld(), IKGoal, 0.1f, FColor::Red, false, 0.0f, 0, 0.1f);

	IKGoal = FMath::Lerp(IKGoal, FilteredIKGoal, KalmanAlpha);

	// LookAt -Z Forward +X Up
	FMatrix IKChainRotation = FRotationMatrix::MakeFromZX(
		(IKGoal - TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation()) * -1, 
		(PoleVector - TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation()));

	FVector RotatedRightFootIKGoal = IKChainRotation.Inverse().TransformVector((IKGoal - TargetSkeleton->GetWorldTransform("RightUpLeg").GetTranslation()));
	FVector SoftenedRightFootIKGoal;
	float RightUpLegStretchFactor;
	SoftIK(FVector::ZeroVector, RotatedRightFootIKGoal, LegLength1 + LegLength2, IKSoftness, 0.0f, SoftenedRightFootIKGoal, RightUpLegStretchFactor);

	float RightUpLegAngle = 0.0f;
	float RightLegAngle = 0.0f;
	IKSolutionFound = CalcIK_2D_TwoBoneAnalytic(false, LegLength1, LegLength2, SoftenedRightFootIKGoal.Z, SoftenedRightFootIKGoal.X, RightUpLegAngle, RightLegAngle);

	FQuat RightUpLegRotation = FQuat::MakeFromEuler(FVector(0.0f, FMath::RadiansToDegrees(RightUpLegAngle), 0.0f));
	FQuat RightLegRotation = FQuat::MakeFromEuler(FVector(0.0f, FMath::RadiansToDegrees(RightLegAngle), 0.0f));

	TargetSkeleton->SetWorldRotation("RightUpLeg", IKChainRotation.ToQuat() * RightUpLegRotation);
	TargetSkeleton->SetLocalRotation("RightLeg", RightLegRotation);
	TargetSkeleton->SetWorldRotation("RightFoot", SourceSkeleton->GetWorldTransform("RightFoot").GetRotation());
}

TMap<FName, FTransform> UNexusMocapBPLibrary::GetLocalTransforms(UProxySkeleton* ProxySkeleton) {
	return ProxySkeleton->BoneLocalTransforms;
}

TMap<FName, FTransform> UNexusMocapBPLibrary::GetWorldTransforms(UProxySkeleton* ProxySkeleton) {
	TMap<FName, FTransform> WorldTransforms;
	for (TPair<FName, FTransform> BoneTransform : ProxySkeleton->BoneLocalTransforms) {
		WorldTransforms.Add(BoneTransform.Key, GetBoneWorldTransform(ProxySkeleton, BoneTransform.Key));
	}
	return WorldTransforms;
}

FTransform UNexusMocapBPLibrary::GetBoneTransformFromSkeletalMesh(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName) {
	int32 BoneIndex = SkeletalMeshComponent->GetBoneIndex(BoneName);
	if (BoneIndex == INDEX_NONE) return FTransform::Identity;
	
	return SkeletalMeshComponent->GetBoneTransform(BoneIndex);
}

FTransform UNexusMocapBPLibrary::GetBoneWorldTransformFromSkeletalMesh(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName) {
	int32 BoneIndex = SkeletalMeshComponent->GetBoneIndex(BoneName);
	if (BoneIndex == INDEX_NONE) return FTransform::Identity;
	FTransform LocalTransform = SkeletalMeshComponent->GetBoneTransform(BoneIndex);
	FName ParentBoneName = SkeletalMeshComponent->GetParentBone(BoneName);
	FVector OutLocation;
	FRotator OutRotator;
	SkeletalMeshComponent->TransformFromBoneSpace(ParentBoneName, LocalTransform.GetTranslation(), LocalTransform.GetRotation().Rotator(), OutLocation, OutRotator);
	return FTransform(OutRotator, OutLocation);
}