// Fill out your copyright notice in the Description page of Project Settings.


#include "ProxySkeleton.h"

TMap<FName, FTransform> BoneLocalTransforms = TMap<FName, FTransform>();
TMap<FName, FName> Parents = TMap<FName, FName>();


// Sets default values for this component's properties
UProxySkeleton::UProxySkeleton()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	BoneLocalTransforms.Add(FName("Reference"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Hips"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftUpLeg"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftLeg"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftFoot"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftToeBase"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightUpLeg"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightLeg"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightFoot"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightToeBase"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Spine"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Spine1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Spine2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Spine3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Neck"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("Head"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftShoulder"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftArm"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftForeArm"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHand"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandIndex1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandIndex2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandIndex3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandMiddle1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandMiddle2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandMiddle3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandRing1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandRing2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandRing3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandPinky1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandPinky2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandPinky3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandThumb1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandThumb2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("LeftHandThumb3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightShoulder"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightArm"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightForeArm"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHand"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandIndex1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandIndex2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandIndex3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandMiddle1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandMiddle2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandMiddle3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandRing1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandRing2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandRing3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandPinky1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandPinky2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandPinky3"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandThumb1"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandThumb2"), FTransform::Identity);
	BoneLocalTransforms.Add(FName("RightHandThumb3"), FTransform::Identity);

	Parents.Add(FName("Hips"), FName("Reference"));
	Parents.Add(FName("LeftUpLeg"), FName("Hips"));
	Parents.Add(FName("LeftLeg"), FName("LeftUpLeg"));
	Parents.Add(FName("LeftFoot"), FName("LeftLeg"));
	Parents.Add(FName("LeftToeBase"), FName("LeftFoot"));
	Parents.Add(FName("RightUpLeg"), FName("Hips"));
	Parents.Add(FName("RightLeg"), FName("RightUpLeg"));
	Parents.Add(FName("RightFoot"), FName("RightLeg"));
	Parents.Add(FName("RightToeBase"), FName("RightFoot"));
	Parents.Add(FName("Spine"), FName("Hips"));
	Parents.Add(FName("Spine1"), FName("Spine"));
	Parents.Add(FName("Spine2"), FName("Spine1"));
	Parents.Add(FName("Spine3"), FName("Spine2"));
	Parents.Add(FName("Neck"), FName("Spine3"));
	Parents.Add(FName("Head"), FName("Neck"));
	Parents.Add(FName("LeftShoulder"), FName("Spine3"));
	Parents.Add(FName("LeftArm"), FName("LeftShoulder"));
	Parents.Add(FName("LeftForeArm"), FName("LeftArm"));
	Parents.Add(FName("LeftHand"), FName("LeftForeArm"));
	Parents.Add(FName("LeftHandIndex1"), FName("LeftHand"));
	Parents.Add(FName("LeftHandIndex2"), FName("LeftHandIndex1"));
	Parents.Add(FName("LeftHandIndex3"), FName("LeftHandIndex2"));
	Parents.Add(FName("LeftHandMiddle1"), FName("LeftHand"));
	Parents.Add(FName("LeftHandMiddle2"), FName("LeftHandMiddle1"));
	Parents.Add(FName("LeftHandMiddle3"), FName("LeftHandMiddle2"));
	Parents.Add(FName("LeftHandRing1"), FName("LeftHand"));
	Parents.Add(FName("LeftHandRing2"), FName("LeftHandRing1"));
	Parents.Add(FName("LeftHandRing3"), FName("LeftHandRing2"));
	Parents.Add(FName("LeftHandPinky1"), FName("LeftHand"));
	Parents.Add(FName("LeftHandPinky2"), FName("LeftHandPinky1"));
	Parents.Add(FName("LeftHandPinky3"), FName("LeftHandPinky2"));
	Parents.Add(FName("LeftHandThumb1"), FName("LeftHand"));
	Parents.Add(FName("LeftHandThumb2"), FName("LeftHandThumb1"));
	Parents.Add(FName("LeftHandThumb3"), FName("LeftHandThumb2"));
	Parents.Add(FName("RightShoulder"), FName("Spine3"));
	Parents.Add(FName("RightArm"), FName("RightShoulder"));
	Parents.Add(FName("RightForeArm"), FName("RightArm"));
	Parents.Add(FName("RightHand"), FName("RightForeArm"));
	Parents.Add(FName("RightHandIndex1"), FName("RightHand"));
	Parents.Add(FName("RightHandIndex2"), FName("RightHandIndex1"));
	Parents.Add(FName("RightHandIndex3"), FName("RightHandIndex2"));
	Parents.Add(FName("RightHandMiddle1"), FName("RightHand"));
	Parents.Add(FName("RightHandMiddle2"), FName("RightHandMiddle1"));
	Parents.Add(FName("RightHandMiddle3"), FName("RightHandMiddle2"));
	Parents.Add(FName("RightHandRing1"), FName("RightHand"));
	Parents.Add(FName("RightHandRing2"), FName("RightHandRing1"));
	Parents.Add(FName("RightHandRing3"), FName("RightHandRing2"));
	Parents.Add(FName("RightHandPinky1"), FName("RightHand"));
	Parents.Add(FName("RightHandPinky2"), FName("RightHandPinky1"));
	Parents.Add(FName("RightHandPinky3"), FName("RightHandPinky2"));
	Parents.Add(FName("RightHandThumb1"), FName("RightHand"));
	Parents.Add(FName("RightHandThumb2"), FName("RightHandThumb1"));
	Parents.Add(FName("RightHandThumb3"), FName("RightHandThumb2"));
	// ...
}


// Called when the game starts
void UProxySkeleton::BeginPlay()
{
	Super::BeginPlay();
	// ...
	
}


// Called every frame
void UProxySkeleton::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...
}

FTransform UProxySkeleton::GetParentTransform(FName BoneName) {
	return GetWorldTransform(GetParent(BoneName));
}

FTransform UProxySkeleton::GetLocalTransform(FName BoneName) {
	return BoneLocalTransforms.FindOrAdd(BoneName);
}

void UProxySkeleton::SetLocalTransform(FName BoneName, FTransform Transform) {
	BoneLocalTransforms.Add(BoneName, Transform);
}

FTransform UProxySkeleton::GetWorldTransform(FName BoneName) {
	FName Parent = GetParent(BoneName);
	if (Parent == BoneName) return GetLocalTransform(BoneName);
	FTransform result;
	result.SetFromMatrix(GetLocalTransform(BoneName).ToMatrixNoScale() * GetWorldTransform(Parent).ToMatrixNoScale());
	//FTransform ParentWorldTransform = GetWorldTransform(Parent);
	//FTransform ParentLocalTransform = GetLocalTransform(Parent);
	//result.SetTranslation(ParentWorldTransform.GetTranslation() + ParentLocalTransform.GetRotation().RotateVector((GetLocalTransform(BoneName).GetTranslation())));
	//result.SetRotation(ParentWorldTransform.TransformRotation(GetLocalTransform(BoneName).GetRotation()));
	return result;
}

FTransform UProxySkeleton::GetWorldTransformTPose(FName BoneName) {
	FName Parent = GetParent(BoneName);
	if (Parent == BoneName) return GetLocalTransform(BoneName);
	FTransform ParentWorldTransform = GetWorldTransformTPose(Parent);
	FTransform LocalTransform = GetLocalTransform(BoneName);
	ParentWorldTransform.SetRotation(FQuat::Identity);
	LocalTransform.SetRotation(FQuat::Identity);
	FTransform result;
	result.SetFromMatrix(LocalTransform.ToMatrixNoScale() * ParentWorldTransform.ToMatrixNoScale());
	return result;
}

FTransform UProxySkeleton::GetLocalTransformTPose(FName BoneName) {
	FName Parent = GetParent(BoneName);
	if (Parent == BoneName) return GetLocalTransform(BoneName);
	FTransform ParentWorldTransform = GetWorldTransformTPose(Parent);
	FTransform LocalTransform = GetLocalTransform(BoneName);
	ParentWorldTransform.SetRotation(FQuat::Identity);
	LocalTransform.SetRotation(FQuat::Identity);
	return LocalTransform * ParentWorldTransform.Inverse();
}


void UProxySkeleton::SetWorldTransform(FName BoneName, FTransform Transform) {
	FTransform LocalTransform = Transform * GetWorldTransform(GetParent(BoneName)).Inverse();
	BoneLocalTransforms.Add(BoneName, LocalTransform);
}

void UProxySkeleton::SetLocalTranslation(FName BoneName, FVector Translation) {
	FTransform LocalTransform = GetLocalTransform(BoneName);
	LocalTransform.SetTranslation(Translation);
	SetLocalTransform(BoneName, LocalTransform);
}
void UProxySkeleton::SetLocalRotation(FName BoneName, FQuat Rotation) {
	FTransform LocalTransform = GetLocalTransform(BoneName);
	LocalTransform.SetRotation(Rotation);
	SetLocalTransform(BoneName, LocalTransform);
}
void UProxySkeleton::SetWorldTranslation(FName BoneName, FVector Translation) {
	FTransform WorldTransform = GetWorldTransform(BoneName);
	WorldTransform.SetTranslation(Translation);
	SetWorldTransform(BoneName, WorldTransform);
}

void UProxySkeleton::SetWorldRotation(FName BoneName, FQuat Rotation) {
	FTransform WorldTransform = GetWorldTransform(BoneName);
	WorldTransform.SetRotation(Rotation);
	SetWorldTransform(BoneName, WorldTransform);
}


FName UProxySkeleton::GetParent(FName BoneName) {
	if (Parents.Contains(BoneName)) {
		return Parents[BoneName];
	}
	return BoneName;
}

TArray<FName> UProxySkeleton::GetChildren(FName BoneName) {
	TArray<FName> Children = TArray<FName>();
	for (TPair<FName, FName> ChildParent : Parents) {
		if (ChildParent.Value == BoneName) Children.Add(ChildParent.Key);
	}
	return Children;
}


void UProxySkeleton::DrawDebugSkeleton(FColor Color = FColor::Black) {
	DrawDebugSkeleton(Color, true, 10.0f);
}

void UProxySkeleton::DrawDebugSkeleton(FColor Color=FColor::Black, bool DrawAxes=true, float AxesLength=10.0f) {

	for (auto& Elem : BoneLocalTransforms) {
		FTransform ChildTransform = GetWorldTransform(Elem.Key);
		FTransform ParentTransform = GetParentTransform(Elem.Key);
		DrawDebugLine(GetWorld(), ChildTransform.GetLocation(), ParentTransform.GetLocation(), Color);
		if (DrawAxes) {
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(), ChildTransform.GetLocation() + ChildTransform.GetRotation().GetRightVector() * AxesLength, FColor::Red);
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(), ChildTransform.GetLocation() + ChildTransform.GetRotation().GetForwardVector() * AxesLength, FColor::Green);
			DrawDebugLine(GetWorld(), ChildTransform.GetLocation(), ChildTransform.GetLocation() + ChildTransform.GetRotation().GetUpVector() * AxesLength, FColor::Blue);
		}
	}
}

