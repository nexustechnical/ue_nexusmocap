// Fill out your copyright notice in the Description page of Project Settings.


#include "KalmanFilter.h"

KalmanFilter::KalmanFilter()
{
}


KalmanFilter3::KalmanFilter3()
{
}


float KalmanFilter::Update(float Value, float NewQ, float NewR) {
	float Q = NewQ;
	float R = NewR;
	float K = (P + Q) / (P + Q + R);
	P = K * R;
	X += (Value - X) * K;
	return X;
}

FVector KalmanFilter3::Update(FVector Value, float NewQ, float NewR) {
	FVector Result = FVector::ZeroVector;
	Result.X = X->Update(Value.X, NewQ, NewR);
	Result.Y = Y->Update(Value.X, NewQ, NewR);
	Result.Z = Z->Update(Value.X, NewQ, NewR);
	return Result;
}

UKalmanFilterFloat::UKalmanFilterFloat() 
{
}

float UKalmanFilterFloat::Update(float value, float NewQ, float NewR)
{
	return K->Update(value, NewQ, NewR);
}



UKalmanFilterVector::UKalmanFilterVector()
{
}

FVector UKalmanFilterVector::Update(FVector value, float NewQ, float NewR)
{
	return K->Update(value, NewQ, NewR);
}