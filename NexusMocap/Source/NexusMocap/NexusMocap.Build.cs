// Some copyright should be here...

using UnrealBuildTool;

public class NexusMocap : ModuleRules
{
	public NexusMocap(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
				"C:/Program Files/Epic Games/UE_4.26/Engine/Plugins/Animation/LiveLink/Source/LiveLink",
				"D:/UNREAL/PluginDev_4_26/Plugins/NexusMocap/Source/ThirdParty/Eigen",
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				// ... add other public dependencies that you statically link with here ...
				"Networking",
				"Sockets",
				"XmlParser",
				"LiveLinkInterface",
				"LiveLinkMessageBusFramework",
				"LiveLink",
				"LiveLinkComponents",
				"Eigen",
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"LiveLink",
				"LiveLinkComponents",
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
