// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "KalmanFilter.h"
#include "Components/ActorComponent.h"
#include "DirectMappingComponent.generated.h"

USTRUCT(BlueprintType)
struct FDirectMapping {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName InputName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float InputZeroValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName OutputName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float OutputMultiplier = 1.0f;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NEXUSMOCAP_API UDirectMappingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDirectMappingComponent();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FDirectMapping> DirectMappings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float KalmanQ;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float KalmanR;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, float> InData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, float> OutData;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void GetNeutralPose();
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void Update();
		
};
