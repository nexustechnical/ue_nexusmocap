// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// #include <iostream>
#include "nxMatrix.h"
#include "Eigen"
#include "UObject/Object.h"
#include "CoreMinimal.h"
//#include "MathUtil.h"
#include <string>
#include <sstream>
#include "Containers/StringConv.h"
//#include "RBF.generated.h"

//#define EPSILON = 0.00000001
using namespace Eigen;
typedef Matrix<double, Dynamic, Dynamic, RowMajor> RowMatrix;
typedef Matrix<double, Dynamic, Dynamic, ColMajor> ColMatrix;

DECLARE_LOG_CATEGORY_EXTERN(LogRBF, Log, All);


UENUM()
enum RBFSmoothingType {
	None,
	InverseMultiQuadraticBiharmonic,
	BeckertWendlandC2Basis
};

//UCLASS(Blueprintable)
class NEXUSMOCAP_API URBF
{

public:
	//URBF(); // nxMatrix sampleArray, nxMatrix outputValues);
	//~URBF();
	void Init(FnxMatrix sampleArray, FnxMatrix outputValues);
	double Distance(TArray<double> vectorA, TArray<double> vectorB);

	TArray<double> Interpolate(TArray<double> newSample);
	static RowMatrix MakeRowMatrixFromnxMatrix(FnxMatrix inMatrix);
	static FnxMatrix MakenxMatrixFromRowMatrix(RowMatrix inMatrix);
	TEnumAsByte<RBFSmoothingType> SmoothingType;
	double smoothingRadius;
private:
	static RowMatrix pseudoInverse(const RowMatrix& a, double epsilon);
//	UPROPERTY()
	FnxMatrix _sampleArray;
//	UPROPERTY()
	FnxMatrix _featureMatrix;
//	UPROPERTY()
	TArray<double> _featureNormals;
//	UPROPERTY()
	FnxMatrix _distances;
//	UPROPERTY()
	FnxMatrix _theta;
//	UPROPERTY()
	FnxMatrix _outputValues;

	double _distancesLength;



	static FString RowMatrixToString(const RowMatrix& mat) {
		std::stringstream ss;
		ss << mat;
		std::string s = ss.str();
		FString fs = s.c_str();
		return fs;
	}

	static FString ColMatrixToString(const ColMatrix& mat) {
		std::stringstream ss;
		ss << mat;
		std::string s = ss.str();
		FString fs = s.c_str();
		return fs;
	}

	static FnxMatrix InverseMultiQuadraticBiharmonic(FnxMatrix matrix, double radius);
	static FnxMatrix BeckertWendlandC2Basis(FnxMatrix matrix, double radius);
	//static VectorXd InverseMultiQuadraticBiharmonic(VectorXd matrix, double radius);
};
