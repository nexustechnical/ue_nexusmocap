// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "KalmanFilter.generated.h"
/**
 * 
 */


class NEXUSMOCAP_API KalmanFilter
{

public:
	KalmanFilter();
	~KalmanFilter();

private:
	float X;
	float P;

public:
	float Update(float Value, float NewQ, float NewR);
};



class NEXUSMOCAP_API KalmanFilter3 
{

public:
	KalmanFilter3();
	~KalmanFilter3();
	FVector Update(FVector Value, float NewQ, float NewR);

private:
	KalmanFilter* X;
	KalmanFilter* Y;
	KalmanFilter* Z;
};

UCLASS(BlueprintType)
class NEXUSMOCAP_API UKalmanFilterFloat : public UObject
{
	GENERATED_BODY()
public:
	UKalmanFilterFloat();
	
private:
	TSharedPtr<KalmanFilter> K;
	
public:
	float Update(float value, float NewQ, float NewR);
	
};

UCLASS(BlueprintType)
class NEXUSMOCAP_API UKalmanFilterVector : public UObject
{
	GENERATED_BODY()
public:
	UKalmanFilterVector();

private:
	TSharedPtr<KalmanFilter3> K;

public:
	FVector Update(FVector value, float NewQ, float NewR);

};

USTRUCT()
struct FKalmanFilterF {
	GENERATED_BODY()

private:
	TSharedPtr<KalmanFilter> K;

public:
	float Update(float value, float NewQ, float NewR) {
		return K->Update(value, NewQ, NewR);
	}
};