// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProxySkeleton.h"
#include "NexusMocap.h"
//#include "BonePose.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "KalmanFilter.h"
#include "RBF.h"
#include "NexusMocapBPLibrary.generated.h"

//#define PI 3.14159

/*
*	Function library class.
*	Each function in it is expected to be static and represents blueprint node that can be called in any blueprint.
*
*	When declaring function you can define metadata for the node. Key function specifiers will be BlueprintPure and BlueprintCallable.
*	BlueprintPure - means the function does not affect the owning object in any way and thus creates a node without Exec pins.
*	BlueprintCallable - makes a function which can be executed in Blueprints - Thus it has Exec pins.
*	DisplayName - full name of the node, shown when you mouse over the node and in the blueprint drop down menu.
*				Its lets you name the node using characters not allowed in C++ function names.
*	CompactNodeTitle - the word(s) that appear on the node.
*	Keywords -	the list of keywords that helps you to find node when you search for it using Blueprint drop-down menu.
*				Good example is "Print String" node which you can find also by using keyword "log".
*	Category -	the category your node will be under in the Blueprint drop-down menu.
*
*	For more info on custom blueprint nodes visit documentation:
*	https://wiki.unrealengine.com/Custom_Blueprint_Node_Creation
*/
UCLASS()
class UNexusMocapBPLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()


	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void SoftIK(FVector rootPosition, FVector ikGoalPosition, float chainLength, float softness, float stretchiness, FVector& ikPosition, float& stretchFactor);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static bool CalcIK_2D_TwoBoneAnalytic(bool flipSolveDirection, float length1, float length2, float targetX, float targetY, float& angle1, float& angle2);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FVector CalculatePoleVectorLocation(FTransform Root, FTransform Bend, float LocalOffset, FVector LocalDirection);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FVector SpaceTransferLocation(FVector Location, FTransform FromSpace, FTransform ToSpace, float ScaleRatio, FRotator Rotation);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FVector SpaceTransferLocationNonUniform(FVector Location, FTransform FromSpace, FTransform ToSpace, FVector ScaleRatio, FRotator Rotation);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FQuat LookAt(FVector Direction, FVector UpVector);
	
	
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FMatrix RotateAlign(FVector v1, FVector v2);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void IKSpaceTransfer(FTransform Root, FTransform Bend, FTransform End, FTransform PVFromSpace, FTransform PVToSpace,
			FTransform IKFromSpace, FTransform IKToSpace, FTransform TargetRoot, FTransform TargetBend, FTransform TargetEnd,
			FQuat& RootRotation, FQuat& BendRotation,
			float ScaleRatio, float PoleVectorLocalOffset, FVector RotationAxis, FQuat OffsetRotation);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FRotator LookAtRotation(FVector lookDirection, FVector upDirection);
	/*
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FRotator LookAtRotation2(FVector lookDirection, FVector upDirection);
		*/


	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void SetBoneWorldTransform(UProxySkeleton *ProxySkeleton, FName BoneName, FTransform Transform);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FTransform GetBoneWorldTransform(UProxySkeleton *ProxySkeleton, FName BoneName);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void SetBoneLocalTransform(UProxySkeleton* ProxySkeleton, FName BoneName, FTransform Transform);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FTransform GetBoneLocalTransform(UProxySkeleton* ProxySkeleton, FName BoneName);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void GetBoneTransform(UProxySkeleton *ProxySkeleton, FName BoneName, FTransform& WorldSpaceTransform, FTransform& LocalSpaceTransform);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FTransform GetBoneParentTransform(UProxySkeleton* ProxySkeleton, FName BoneName);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static FName GetBoneParent(UProxySkeleton* ProxySkeleton, FName BoneName);
	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TArray<FName> GetBoneChildren(UProxySkeleton* ProxySkeleton, FName BoneName);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void GetChainToBone(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName, FName ParentBoneName, TArray<FName>& Chain, bool& Found);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void GetJointRotationsInTPose(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton, TMap<FName, FQuat>& JointRotations);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FQuat GetJointWorldRotationInTPose(FName BoneName, USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void GetJointWorldRotationsInTPose(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton, TMap<FName, FQuat>& JointRotations);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void InitProxySkeleton(UProxySkeleton* ProxySkeleton);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void DrawProxySkeleton(UProxySkeleton *ProxySkeleton, FColor Color);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void SetProxyFromAnimSequence(UProxySkeleton* ProxySkeleton, UAnimSequence* TPose);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static float GetHipsRatio(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, UProxySkeleton* ProxySkeleton);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void SetProxyFromSkeletalMesh(UProxySkeleton* ProxySkeleton, USkeletalMeshComponent* SkeletalMeshComponent);
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void SetProxyBoneLengthFromSkeletalMesh(UProxySkeleton* ProxySkeleton, USkeletalMeshComponent* SkeletalMeshComponent, TMap<FName, float>& BoneLengthRatios, UAnimSequence* TPoseAnimation);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FTransform GetLocalTransformFromAnimSequence(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, FName BoneName, float AtTime);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FTransform GetWorldTransformFromAnimSequence(USkeletalMeshComponent* SkeletalMeshComponent, UAnimSequence* AnimSequence, FName BoneName, float AtTime);
	
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static int32 GetAnimSequenceBoneIndex(UAnimSequence* MyAnimSequence, USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FTransform GetBoneTransformAtTime(UAnimSequence* MyAnimSequence, float AnimTime, int BoneIdx);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TMap<FName, FTransform> GetLocalTransforms(UProxySkeleton* ProxySkeleton);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TMap<FName, FTransform> GetWorldTransforms(UProxySkeleton* ProxySkeleton);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static float GetArmLength(UProxySkeleton* ProxySkeleton);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void GetFootHeightRatio(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, float& SourceFootHeight, float& FootHeightRatio);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void IKSpaceTransferLeftHand(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha,
			float& ArmRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float& StretchFactor, bool DebugDraw, bool FlipSolveDirection, bool InvertForwardAxis, bool InvertUpAxis, bool InvertAngle1, UKalmanFilterVector* KalmanFilter);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void IKSpaceTransferRightHand(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha,
			float& ArmRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float& StretchFactor, bool DebugDraw, bool FlipSolveDirection, bool InvertForwardAxis, bool InvertUpAxis, bool InvertAngle1, UKalmanFilterVector* KalmanFilter);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void IKSpaceTransferLeftFoot(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha, float HipsRatio, float FootHeightRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float footHeightFallout, float SourceFootHeight, bool DebugDraw, UKalmanFilterVector* KalmanFilter);
	
	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static void IKSpaceTransferRightFoot(UProxySkeleton* SourceSkeleton, UProxySkeleton* TargetSkeleton, FVector PoleVectorLocalOffset, float IKSoftness, float Stretchiness, float KalmanAlpha, float HipsRatio, float FootHeightRatio, FVector& PoleVector, FVector& IKGoal, bool& IKSolutionFound, float footHeightFallout, float SourceFootHeight, bool DebugDraw, UKalmanFilterVector* KalmanFilter);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FTransform GetBoneTransformFromSkeletalMesh(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		static FTransform GetBoneWorldTransformFromSkeletalMesh(USkeletalMeshComponent* SkeletalMeshComponent, FName BoneName);

	//static TMap<FName, double> 

	//UFUNCTION(BlueprintCallable, Category = "NexusMocap")
		//static void ProxySkeletonToPose(UProxySkeleton* ProxySkeleton, USkeletalMeshComponent* SkeletalMesh, FCompactPose& OutPose);

	static float KalmanQ;

	static float KalmanR;

};


