// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class NEXUSMOCAP_API TransformHierarchy
{
public:
	TransformHierarchy();
	~TransformHierarchy();

	TMap<FName, FTransform*> Transforms;
	TMap<FName, FName> Parents;


	FTransform GetLocalTransform(FName BoneName);
	FTransform* GetParentTransform(FName BoneName);
	FTransform* GetWorldTransform(FName BoneName);
	void SetWorldTransform(FName BoneName, FTransform Transform);

	void SetupTransformsByName();
	void SetupParents();
};
