#pragma once


#include "nxMatrix.generated.h"

USTRUCT()
struct FnxMatrix
{
	GENERATED_BODY()
public:
	FnxMatrix();

	FnxMatrix(size_t numrows, size_t numcols);// :
	FnxMatrix(TArray<double> vector);// :
private:
	UPROPERTY()
	int rows;
	UPROPERTY()
	int columns;
public:
	UPROPERTY()
	TArray<double> matrix;
	
	/*
	rows(numrows), columns(numcols), matrix(0, numrows* numcols)
	{
	}
	*/

	double& operator()(int row, int column)
	{
		return matrix[row * columns + column]; // note 2D coordinates are flattened to 1D
	}

	double operator()(int row, int column) const
	{
		return matrix[row * columns + column];
	}


	size_t getRows() const
	{
		return rows;
	}

	size_t getColumns() const
	{
		return columns;
	}

	FString ToString() {
		FString outputString = "";
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {
				outputString += FString::SanitizeFloat(this->operator()(r, c));
				outputString += ", ";
			} 
			outputString += "\n";
		}
		return outputString;
	}

};