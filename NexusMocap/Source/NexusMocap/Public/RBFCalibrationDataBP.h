// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
////#include "RBF.h"
#include "RBFCalibration.h"
#include "RBFCalibrationDataBP.generated.h"

/**
 * 
 * 
 */
UCLASS()
class NEXUSMOCAP_API URBFCalibrationDataBP : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()

};
