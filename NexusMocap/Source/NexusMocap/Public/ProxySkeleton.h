// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ProxySkeleton.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NEXUSMOCAP_API UProxySkeleton : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UProxySkeleton();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;



	UPROPERTY(EditAnywhere)
	TMap<FName, FTransform> BoneLocalTransforms;

	UPROPERTY(EditAnywhere)
	TMap<FName, FName> Parents;

	FName GetParent(FName BoneName);
	TArray<FName> GetChildren(FName BoneName);

	FTransform GetLocalTransform(FName BoneName);
	FTransform GetParentTransform(FName BoneName);
	FTransform GetWorldTransform(FName BoneName);
	FTransform GetWorldTransformTPose(FName BoneName);
	FTransform GetLocalTransformTPose(FName BoneName);

	void SetLocalTransform(FName BoneName, FTransform Transform);
	void SetWorldTransform(FName BoneName, FTransform Transform);
	void SetLocalTranslation(FName BoneName, FVector Translation);
	void SetLocalRotation(FName BoneName, FQuat Rotation);
	void SetWorldTranslation(FName BoneName, FVector Translation);
	void SetWorldRotation(FName BoneName, FQuat Rotation);
	
	void SetupTransformsByName();
	void SetupParents();

	void DrawDebugSkeleton(FColor color);
	void DrawDebugSkeleton(FColor color, bool DrawAxes, float AxesLength);
};

