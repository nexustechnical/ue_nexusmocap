// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NexusMocap.h"
#include "RBF.h"
#include "KalmanFilter.h"
//#include "RBFCalibrationDataBP.h"
#include "Components/ActorComponent.h"
#include "CurveRBFComponent.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRBFComponent, Log, All);

USTRUCT(BlueprintType)
struct FNamedChannel {
	GENERATED_BODY()
public:

	FNamedChannel(FName Name = "", float Weight = 0.0f) : Name(Name), Weight(Weight) {}
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Weight;
};

USTRUCT(BlueprintType)
struct FShape {
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FNamedChannel> InputChannels;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FNamedChannel> OutputChannels;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NEXUSMOCAP_API UCurveRBFComponent : public UActorComponent
{
	GENERATED_BODY()
public:
	//UPROPERTY()
	URBF Rbf;

	// Sets default values for this component's properties
	UCurveRBFComponent();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FName, FShape> CalibrationShapes;
	//TArray<KalmanFilter> KalmanFilters;

	UPROPERTY(VisibleAnywhere)
	TArray<double> RBFOutputs;
	UPROPERTY(EditAnywhere)
	TEnumAsByte<RBFSmoothingType> SmoothingType;
	UPROPERTY(EditAnywhere)
	double SmoothingRadius;

	UPROPERTY(EditAnywhere)
	TArray<FName> PositiveBlendShapeOutputs;
	UPROPERTY(EditAnywhere)
	TArray<FName> NegativeBlendShapeOutputs;
	UPROPERTY(BlueprintReadWrite)
	TMap<FName, float> InData;
	UPROPERTY(BlueprintReadOnly)
	TMap<FName, float> OutData;
	UPROPERTY(EditAnywhere)
	double KalmanQ;
	UPROPERTY(EditAnywhere)
	double KalmanR;
	UPROPERTY(BlueprintReadOnly)
	bool isSetup;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void PruneInDataToChannels(TSet<FName> Channels, TMap<FName, float> InputChannels, TMap<FName, float>& PrunedInputChannels);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static void CalibrateShape(FName ShapeName, TMap<FName, float> InputChannels, TArray<FNamedChannel> OutputChannels, FShape& OutShape);

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void SetupRBF();

	UFUNCTION(BlueprintCallable, Category = "NexusMocap")
	void UpdateRBF();

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TArray<FNamedChannel> CurvesMapToNamedChannelArray(TMap<FName, float> Curves);

	UFUNCTION(BlueprintPure, Category = "NexusMocap")
		static TMap<FName, float> NamedChannelArrayToCurvesMap(TArray<FNamedChannel> Curves);
};
